var totalDamage = {};

$(document).ready(function() {
	setDarkreaderWatcher();
	$('#addDamageSource').click(addDamageSource);
	$('#btnRoll').click(roll);
	$('[min]').blur(function() {
		var min = $(this).attr('min');
		if ($(this).val() < min)
			$(this).val('');
	});
	$('[data-js-collapse]').change(collapse);
	$('[data-js-ac-range]').on('input', delay(checkIfHitMiss, 300));
});

function setDarkreaderWatcher() {
	var className = 'dark-reader';
	if (isDarkreaderOn())
		document.body.classList.add(className);
	(new MutationObserver(() =>
		document.body.classList.toggle(className, isDarkreaderOn())).observe(document.querySelector('head'),
		{ childList: true }));
}

function isDarkreaderOn() {
	return document.querySelector('meta[name="darkreader"]');
}

function collapse() {
	var id = $(this).attr('data-js-collapse');
	if ($(this).is(':checked'))
		$('#' + id).css('display', 'block');
	else
		$('#' + id).css('display', 'none');
}

function addDamageSource() {
	var damageDiceLabel = $(document.createElement('label'))
		.text('Damage dice');
	var damageDiceInput = $(document.createElement('input'))
		.attr('data-js-dmg-dice', '')
		.attr('type', 'text')
		.attr('placeholder', '2d8 + 3');
	var damageDiceDiv = $(document.createElement('div'))
		.append(damageDiceLabel)
		.append(damageDiceInput);

	var damageTypeLabel = $(document.createElement('label'))
		.text('Damage type');
	var damageTypeInput = $(document.createElement('select'))
		.attr('data-js-dmg-type', '')
		.append($(document.createElement('option')));
	$.each(resources.DamageType, function (index, value) {
		var option = $(document.createElement('option'))
			.text(value)
			.val(value);
		damageTypeInput.append(option);
	});
	var damageTypeDiv = $(document.createElement('div'))
		.append(damageTypeLabel)
		.append(damageTypeInput);

	var removeButton = $(document.createElement('button'))
		.text('Remove damage source')
		.attr('type', 'button')
		.click(removeDamageSource);

	var damageSource = $(document.createElement('div'))
		.attr('data-js-dmg-src', '')
		.addClass('dmgSrc')
		.append(damageDiceDiv)
		.append(damageTypeDiv)
		.append(removeButton);

	$('#secDamageDice').append(damageSource);
	
	damageDiceInput.focus();
};

function removeDamageSource() {
	$(this).parent().remove();
}

function roll() {
	$('#rollTable')
		.removeClass('hidden');
	$('#tblBody').empty();

	var amount = parseInt($('#amount').val() || 0);
	var amountAdvantage = parseInt($('#amountAdvantage').val() || 0);
	var amountDisadvantage = parseInt($('#amountDisadvantage').val() || 0);
	var toHitModifier = parseInt($('#toHitModifier').val() || 0);
	
	var acMin = $('#acMin').val();
	var acMax = $('#acMax').val();

	var dmgObjects = getDamageObjects();

	rollFlat(amount, dmgObjects, toHitModifier, acMin, acMax);
	rollAdvantage(amountAdvantage, dmgObjects, toHitModifier, acMin, acMax);
	rollDisadvantage(amountDisadvantage, dmgObjects, toHitModifier, acMin, acMax);

	calculateTotal();
}

function getDamageObjects() {
	var arr = [];

	$.each($('[data-js-dmg-src]'), function (index, value) {
		var damageDiceString = $(value)
			.find('[data-js-dmg-dice]')
			.val()
			.replace(/\s/g, '');
		if (damageDiceString.includes('d')) {
			var splitOnD = damageDiceString.split('d');
			var damageDiceCount = splitOnD[0] || '1';
			var splitOnPlus = splitOnD[1].split('+');
			if (splitOnPlus.length < 2)
				var splitOnMinus = splitOnD[1].split('-');
			if (splitOnMinus) {
				var damageDiceType = splitOnMinus[0];
				var damageModifier = splitOnMinus[1] * -1;
			}
			else {
				var damageDiceType = splitOnPlus[0];
				var damageModifier = splitOnPlus[1];
			}
		} else {
			var damageModifier = damageDiceString;
		}

		var damageType = $(value)
			.find('[data-js-dmg-type]')
			.val();

		arr.push({
			count: parseInt(damageDiceCount || 0),
			diceType: parseInt(damageDiceType || 0),
			modifier: parseInt(damageModifier || 0),
			type: damageType
		});
	});

	return arr;
}

function rollFlat(amount, dmgObjects, toHitModifier, acMin, acMax) {
	for (var i = 0; i < amount; i++) {
		var diceRoll = rollDie(20);
		rollAllDamage(diceRoll, dmgObjects, toHitModifier, acMin, acMax, diceRoll, undefined, false, false);
	}
}

function rollAdvantage(amount, dmgObjects, toHitModifier, acMin, acMax) {
	for (var i = 0; i < amount; i++) {
		var diceRoll1 = rollDie(20);
		var diceRoll2 = rollDie(20);
		var diceRoll = Math.max(diceRoll1, diceRoll2);
		rollAllDamage(diceRoll, dmgObjects, toHitModifier, acMin, acMax, diceRoll1, diceRoll2, true, false);
	}
}

function rollDisadvantage(amount, dmgObjects, toHitModifier, acMin, acMax){
	for (var i = 0; i < amount; i++) {
		var diceRoll1 = rollDie(20);
		var diceRoll2 = rollDie(20);
		var diceRoll = Math.min(diceRoll1, diceRoll2);
		rollAllDamage(diceRoll, dmgObjects, toHitModifier, acMin, acMax, diceRoll1, diceRoll2, false, true);
	}
}

function rollAllDamage(diceRoll, dmgObjects, toHitModifier, acMin, acMax, diceRoll1, diceRoll2, advantage, disadvantage) {
	var crits = diceRoll === 20;
	var critMiss = diceRoll === 1;
	var attackRoll = diceRoll + toHitModifier;

	var hitConfirmed = false;
	var missConfirmed = false;
	if ((acMin && attackRoll <= acMin) || critMiss) {
		missConfirmed = true;
	}
	if ((acMax && attackRoll >= acMax) || crits) {
		hitConfirmed = true;
	}
	if (critMiss)
		hitConfirmed = false;
	if (crits)
		missConfirmed = false;

	var damage = rollDamage(dmgObjects, crits);

	addAttackRow(attackRoll, damage, crits, critMiss, hitConfirmed, missConfirmed, diceRoll1, diceRoll2, toHitModifier, advantage, disadvantage);
}

function rollDamage(dmgObjects, crit) {
	var clonedDmgObjects = JSON.parse(JSON.stringify(dmgObjects))

	var result = [];

	$.each(clonedDmgObjects, function (index, obj) {
		if (crit)
			obj.count = obj.count * 2;

		var rolls = [];
		for (var i = 0; i < obj.count; i++) {
			rolls.push(rollDie(obj.diceType));
		}

		result.push({
			rolls: rolls,
			type: obj.type,
			modifier: obj.modifier
		});
	});

	return result;
}

function rollDie(max) {
  return Math.floor(Math.random() * max + 1)
}

function addAttackRow(attackRoll, damage, crit, critMiss, hitConfirmed, missConfirmed, diceRoll1, diceRoll2, toHitModifier, advantage, disadvantage) {
	if (diceRoll2)
		var atckTltpTxt = '(' + diceRoll1 + ',' + diceRoll2 + ')';
	else
		var atckTltpTxt = diceRoll1;
	if (toHitModifier !== 0)
		atckTltpTxt += ' + ' + toHitModifier;

	var tdAdvDisadv = $(document.createElement('td'));
	if (advantage) {
		var spanAdvantage = $(document.createElement('span'))
			.addClass('notFlatRoll')
			.addClass('floatRight');
		$('#iconAdvantage').clone().appendTo(spanAdvantage);
		tdAdvDisadv.append(spanAdvantage);
	} else if (disadvantage) {
		var spanDisadvantage = $(document.createElement('span'))
			.addClass('notFlatRoll')
			.addClass('floatRight');
		$('#iconDisadvantage').clone().appendTo(spanDisadvantage);
		tdAdvDisadv.append(spanDisadvantage);
	}

	var spanAttackCustomTooltip = $(document.createElement('span'))
		.addClass('customTooltiptext')
		.text(atckTltpTxt);
	var spanAttack = $(document.createElement('span'))
		.text(attackRoll)
		.attr('data-js-attackRoll', '');
	var tdAttack = $(document.createElement('td'))
		.addClass('customTooltip')
		.append(spanAttack)
		.append(spanAttackCustomTooltip);

	var dmgStr = '';
	var dmgTltpTxt = '';
	var rowDamage = {};
	$.each(damage, function (index, value) {
		if (index > 0) {
			dmgStr += ', ';
			dmgTltpTxt += '<br />';
		}
		var amount = value.rolls.reduce((a, b) => a + b, 0) + value.modifier;
		dmgStr += amount + ' ' + value.type;
		if (value.rolls.length > 0)
			dmgTltpTxt += value.rolls.reduce((a, b) => a + '+' + b);
		if (value.modifier !== 0)
			dmgTltpTxt += ' + ' + value.modifier;
		dmgTltpTxt += ' ' + value.type;

		if (rowDamage[value.type])
			rowDamage[value.type] += amount;
		else
			rowDamage[value.type] = amount;
	});
	var spanDamageCustomTooltip = $(document.createElement('span'))
		.addClass('customTooltiptext')
		.html(dmgTltpTxt);
	var spanDamage = $(document.createElement('span'))
		.text(dmgStr)
		.attr('data-js-damage', JSON.stringify(rowDamage));
	var tdDamage = $(document.createElement('td'))
		.addClass('customTooltip')
		.append(spanDamage)
		.append(spanDamageCustomTooltip);
	if (dmgTltpTxt.trim() === '')
		spanDamageCustomTooltip.remove();

	var tr = $(document.createElement('tr'))
		.append(tdAdvDisadv)
		.append(tdAttack)
		.append(tdDamage)
		.attr('data-js-row', '');
	if (crit)
		tr.addClass('crit');
	else if (critMiss)
		tr.addClass('critMiss');
	if (hitConfirmed)
		tr.addClass('hits');
	if (missConfirmed)
		tr.addClass('misses');

	$('#tblBody').append(tr);
}

function checkIfHitMiss() {
	var acMin = $('#acMin').val();
	var acMax = $('#acMax').val();

	$.each($('[data-js-row]'), function (index, row) {
		$(row).removeClass('hits');
		$(row).removeClass('misses');
		var attackRoll = parseInt($(row).find('[data-js-attackRoll]').text());
		var hitConfirmed = false;
		var missConfirmed = false;
		var crit = $(row).hasClass('crit');
		var critMiss = $(row).hasClass('critMiss');
		if ((acMin && attackRoll <= acMin) || critMiss)
			missConfirmed = true;
		if ((acMax && attackRoll >= acMax) || crit)
			hitConfirmed = true;
		if (critMiss)
			hitConfirmed = false;
		if (crit)
			missConfirmed = false;

		if (hitConfirmed)
			$(row).addClass('hits');
		if (missConfirmed)
			$(row).addClass('misses');
	});

	calculateTotal();
}

function calculateTotal() {
	totalDamage = {};
	$.each($('[data-js-row].hits').find('[data-js-damage]'), function (index, value) {
		var dmgObj = JSON.parse($(value).attr('data-js-damage'));
		for (var key in dmgObj) {
			if (totalDamage[key])
				totalDamage[key] += dmgObj[key];
			else
				totalDamage[key] = dmgObj[key];
		}

		var str = '';
		var i = 0;
		for (var key in totalDamage) {
			if (i > 0)
				str += ', ';
			str += key + ': ' + totalDamage[key];
			i++;
		}

		$('#totalDamage').text(str);
	});

	if ($('[data-js-row].hits').length === 0)
		$('#totalDamage').text(0);
}

function delay(callback, ms) {
    var timer = 0;
    return function() {
        var context = this, args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function () {
            callback.apply(context, args);
        }, ms || 0);
    };
}
